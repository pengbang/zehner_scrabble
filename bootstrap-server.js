/**
 * Created by pengbang on 09.06.2016.
 */
var System = require('es6-module-loader').System;

System.import('./server').then(function(index) {
    index.run(__dirname);
}).catch(function(err){
    console.log('err', err);
});