/** Main app for server to start a small REST API for videos
 * The included ./blackbox/store.js gives you access to a "database" which contains
 * nothing this time.
 * On each restart the db will be reset (it is only in memory).
 *
 * Note: set your environment variables
 * NODE_ENV=development
 * debug=me2u4:*
 *
 * @author Johannes Konert
 * @edited Yeong-Cheol Jang, Michael Alexandrakis
 * @licence CC BY-SA 4.0
 *
 */


"use strict";
let config = require('./config')
let LIST_LENGTH = config.LIST_LENGTH;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var requestLogger = require('morgan');
let fs = require('fs');
// own modules
var restAPIchecks = require('./restapi/request-checks.js');


// app creation
var app = express();
let debug = (e, a) => console.log(e, a)
let wordlistTen = fs.readFileSync('./data/reduced.dic').toString().split("\n");
let wordlistAll = fs.readFileSync('./data/allUpperCased.dic').toString().split("\n");
let scrambledwordlist = fs.readFileSync('./data/scrambled.dic').toString().split("\n");
// Middlewares *************************************************

// app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

// logging
app.use(requestLogger('dev'));

// API request checks for API-version and JSON etc.
app.use(restAPIchecks);

app.use(function (req, res, next) {
    res.header("Content-Type", "application/json");
    /**
     * @TODO
     * CHANGE *
     */
    res.header("Access-Control-Allow-Origin", "*");
    next();
});
// Routes ******************************************************

app.get('/word/:id', (req, res) => {

    res.json(wordlistTen[req.params.id])
})
app.get('/scrambled/:id', (req, res) => {

    res.json(scrambledwordlist[req.params.id])
})
app.get('/:id/test', (req, res) => {

    res.json(scrambledwordlist[req.params.id])
})
app.get('/randomId', (req, res)=> res.json(Math.floor(Math.random() * LIST_LENGTH)))
app.get('/exists/:word', (req, res)=> res.json(wordlistAll.indexOf(req.params.word) > -1 ? {solved: true} : {solved: false}))


// (from express-generator boilerplate  standard code)
// Errorhandling and requests without proper URLs ************************
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    console.log('Catching unmatched request to answer with 404');
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// error handlers (express recognizes it by 4 parameters!)
// development error handler
// will print stacktrace as JSON response
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        debug('Server responds with error: ', err.stack);
        res.status(err.status || 500);
        res.json({
            error: {
                message: err.message,
                error: err.stack,
                code: err.status || 500
            }
        });
    });
} else {
    // production error handler
    // no stacktraces leaked to user
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            error: {
                message: err.message,
                error: {},
                code: err.status || 500
            }
        });
    });
}
// Start server ****************************
app.listen(3001, function (err) {
    if (err !== undefined) {
        console.log('Error on startup, ', err);
    }
    else {
        console.log('Listening on port 3001');
    }
});