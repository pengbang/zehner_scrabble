
module.exports = {

    entry:
    {
        main:'./src/main.js'
    },
    output: {
        path: __dirname,
        publicPath: '/public/resources',
        filename: 'index.js',
        sourceMapFilename : "index.map"
    },
    devtool: '#source-map',
    devServer : {
        hot: true,
        inline:true,
        port: 8000,
        colors: true
    },
    module: {
        loaders: [
            {
                test:/\.jsx?$/,
                exclude:/node_modules/,
                // include: __dirname,
                loader:'babel',
                query: {
                    presets: ['es2015','es2016','react'],

                    plugins: ["transform-object-rest-spread"]

            }
            }
        ]
    }
}

function getEntrySources(sources) {
    if (process.env.NODE_ENV !== 'production') {
        // ...
        sources.push('webpack/hot/only-dev-server');
    }

    // ...
}