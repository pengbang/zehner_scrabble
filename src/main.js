import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import DevTools from './dev/DevTools';
import {log} from './dev/DevTools';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MainSheet from './components/mainsheet'

import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const muiTheme = getMuiTheme(lightBaseTheme);


import store  from './store'
log("muiTHeme : ", muiTheme)


const rootElement = document.getElementById('main');
let prov = (
    <MuiThemeProvider muiTheme={muiTheme}>
    <Provider store={store}>
        <MainSheet/>
    </Provider>
    </MuiThemeProvider>)

//necessary for material-ui
injectTapEventPlugin();
ReactDOM.render(prov, rootElement);


/**
 * FOR DEVELOPMENT ONLY
 *
 */
window.React = React;
const devElement = document.getElementById('devtools');
ReactDOM.render(<DevTools store={store}/>, devElement);