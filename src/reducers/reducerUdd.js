import {TEST} from '../actions/apiActions'
import {
    SIDEBAR_OPEN,
    REMOVE_GUESS,
    MAKE_GUESS,
    RECEIVE_WORD,
    GET_WORD_ID,
    RECEIVE_WORD_ID,
    SCRAMBLE_AVAILABLE,
    RECEIVE_SOLVED,
    CLEAR_GAME_AREA,
    SOLVE_ROUND
} from '../actions/baseActions'
import {log} from '../dev/DevTools';
import {LIST_LENGTH} from '../../config'
import fs from 'fs';
/**##################################################
 * #                                                #
 * #           REDUCER TEST / BOILERPLATE FILE      #
 * #                                                #
 * #################################################*/
let initialState = {

    data: "WELT!!",
    open: true,
    currentWordRow: 1,

    gameState: {
        word: "test",
        available: [],
        guessed: [],
        solved: false,
        checking: false,
        points: 0
    }

}
export default function (state = initialState, action) {
    switch (action.type) {
        case TEST :
        {
            let e = (action.data) ? action.data.data : "kein inhalt";
            log("test reducer");
            return Object.assign({}, state, {data: e});
            // return {...state, data : "testWorld"}
        }
            break;
        case SIDEBAR_OPEN :
        {

            log("reducerUDD : SIDEBAR_OPEN ");
            return Object.assign({}, state, {open: !action.open});
            // return {...state, data : "testWorld"}
        }
            break;
        case GET_WORD_ID :
        {
            return Object.assign({}, state, {currentWordRow: Math.floor(Math.random() * LIST_LENGTH)})
        }
            break;
        case RECEIVE_WORD :
        {
            // var array = fs.readFileSync('../../data/reduced.dic').toString().split("\n");
            log("get_word")
            // return Object.assign({}, state,{solution : array[state.currentWordRow]})
            return Object.assign({}, state,
                {

                    gameState: {
                        word: action.payload.data,
                        available: [...action.payload.data.split('').sort()],
                        guessed: [],
                        solved: false,
                        checking: false,
                        points: state.gameState.points
                    }
                }
            );

        }
            break;
        case RECEIVE_WORD_ID :
        {
            return Object.assign({}, state, {currentWordRow: action.receivedWordId})
        }
            break;
        case MAKE_GUESS :
        {
            return Object.assign({}, state,
                {
                    gameState: {
                        word: state.gameState.word,

                        available: [
                            ...state.gameState.available.slice(0, action.index),
                            ...state.gameState.available.slice(action.index + 1)

                        ],
                        guessed: [...state.gameState.guessed, action.letter],
                        solved: false,
                        checking: false,
                        points: state.gameState.points
                    }
                })
        }
            break;
        case REMOVE_GUESS :
        {
            return Object.assign({}, state,
                {
                    gameState: {
                        word: state.gameState.word,

                        available: [
                            ...state.gameState.available, action.letter

                        ],
                        guessed: [
                            ...state.gameState.guessed.slice(0, action.index),
                            ...state.gameState.guessed.slice(action.index + 1)
                        ],
                        solved: false,
                        checking: false,
                        points: state.gameState.points
                    }
                })
        }
            break;
        case SCRAMBLE_AVAILABLE:
        {
            // console.log("trying to scramble", state)
            return {
                ...state,
                    gameState: {
                    ...state.gameState,
                        available: [...state.gameState.available].sort((a, b)=> Math.floor(Math.random() * 3) - 1),
                        guessed: [...state.gameState.guessed]
                    }
                }
        }
            break;
        case RECEIVE_SOLVED :
        {
            return  {
                    ...state,
                    gameState: {
                        ...state.gameState,

                        solved: action.solved,
                        checking: true,
                        points: action.solved ?
                            state.gameState.guessed.length == state.gameState.word.length ?
                            state.gameState.points + state.gameState.guessed.length + 5 : state.gameState.points + state.gameState.guessed.length
                            : state.gameState.points
                    }
                }
        }
            break;
        case CLEAR_GAME_AREA :
        {
            return Object.assign({}, state,
                {
                    gameState: {
                        word: state.gameState.word,
                        available: [...state.gameState.available, ...state.gameState.guessed],
                        guessed: [],
                        solved: false,
                        checking: false,
                        points: state.gameState.points
                    }
                })
        }
            break;
        case SOLVE_ROUND : {
            return Object.assign({},state, {
                gameState: {
                    word: state.gameState.word,
                    available: [],
                    guessed: [...state.gameState.word.split('')],
                    solved: true,
                    checking: true,
                    points: state.gameState.points
                }
            })
        }
        default :

            log("default reducer", state)
            // return Object.assign({}, state, {data: "WEREAS"});

            return state


    }
}