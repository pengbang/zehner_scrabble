// /**
//  * Created by pengbang on 28.04.2016.
//  */
//

import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux'
import reducerUdd from './reducerUdd';

//all new Reducers shall be imported here
export default  combineReducers({
        reducerUdd,
        routing: routerReducer

});
