import React from 'react';
import store from '../store';
import {connect} from 'react-redux';
// import {Paper}  from 'material-ui';
import RaisedButton from 'material-ui/RaisedButton'
 import {GET_WORD,fetchData,makeGuess,removeGuess} from '../actions/baseActions'


class LetterComponent extends React.Component {
    constructor(props){
        super(props)
    }
    onClick() {
        // console.log("onclicking letter",this)
        switch (this.props.type){
            case "user": store.dispatch(makeGuess({letter:this.props.letter, index:this.props.index})); break;
            case "guess": store.dispatch(removeGuess({letter:this.props.letter, index:this.props.index})); break;
            default: console.log("oooops")
        }
    }
    handleClass(){
        // console.log("trying to",this.props.guessed.length,this.props.word.length)
        // if (this.props.type == "guess") {
        //       return this.props.solved ?  "letter green" : (this.props.checking)? "letter white" : "letter red";
        // }
        // return
        console.log("trying to",this.props.guessed.length,this.props.word.length)
        switch(this.props.type) {
            case "guess" : return this.props.solved ?  "letter green" : (this.props.checking)? "letter red" : "letter white";break;
            default :return"letter white"
        }
    }
    render() {
        const {type,checking,solved,letter } = this.props
        let style = {
            marginRight: "10px",
            width :"88px",
            height :"88px",

        }
        let inlineStyle= {
            width :"88px",
            lineHeight :"88px",
            backgroundColor:
                type =="guess" ?
                    checking ?
                        solved ? "#A5D6A7": "#ef9a9a"
                        : "inherit"
                    :"inherit",
            textAlign : "center",
            fontWeight : "bold",
            fontSize : "40px",
            display:"block",
            position:"relative",
            padding : "0 0 0 0",
            margin : "0 0 0 0"
        }

        return (
            <RaisedButton style={style}  labelStyle={inlineStyle}
            onClick={this.onClick.bind(this)} label={letter}/>)
    }
}
const mapStateToProps = (state) =>{
    return {
        muiTheme: state.muiTheme,
        word : state.reducerUdd.gameState.word,
        available: state.reducerUdd.gameState.available,
        guessed: state.reducerUdd.gameState.guessed,
        gameState : state.reducerUdd.gameState,
        solved : state.reducerUdd.gameState.solved,
        checking : state.reducerUdd.gameState.checking

    }
}
LetterComponent.contextTypes = {
    muiTheme : React.PropTypes.object,
    store : React.PropTypes.object,
    word : React.PropTypes.string,
    available : React.PropTypes.arrayOf(React.PropTypes.string),
    guessed : React.PropTypes.arrayOf(React.PropTypes.string),
    letter  : React.PropTypes.string,
    key: React.PropTypes.number,
    index  : React.PropTypes.number,
    gameState : React.PropTypes.object,
    solved :React.PropTypes.bool,
    checking : React.PropTypes.bool,
    type : React.PropTypes.string,

}
export default connect(mapStateToProps)(LetterComponent);