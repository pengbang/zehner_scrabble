import  React from 'react';
import store from '../store'
import UserArea from './areas/user-area'
import GameArea from './areas/game-area'
import ControlArea from './areas/control-area'
import {connect} from 'react-redux';
import {GET_WORD,fetchData,fetchRandomWord} from '../actions/baseActions'


class MainSheet extends React.Component {
    constructor(props){
        super(props)
    }
    componentWillMount(){
        store.dispatch(fetchRandomWord())
    }
    render() {
        let styles= {
            paddingTop : "50px"
        }
        return (
            <div className="wrapper">
                <GameArea style={styles}/>
                <ControlArea/>
                <UserArea style={styles}/>
            </div>)
    }
}
const mapStateToProps = (state) =>{
    return {
        muiTheme: state.muiTheme,
        store: state.store
    }
}
MainSheet.contextTypes = {
    muiTheme : React.PropTypes.object,
    store : React.PropTypes.object
}
export default connect(mapStateToProps)(MainSheet);