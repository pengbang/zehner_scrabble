import React from 'react'
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import store from '../../store';
import {scrambleAvailable,wordExistRequest,clearGameArea,solveRound} from '../../actions/baseActions'

class ControlArea extends React.Component {
    constructor(props){
        super(props)
    }
    onScramble(){
         store.dispatch(scrambleAvailable())
    }
    onSubmit(){

        store.dispatch(wordExistRequest({possibleSolution:this.props.guessed.join('')}))
    }
    onClear(){
        store.dispatch(clearGameArea())
    }
    onSolve(){
        store.dispatch(solveRound())
    }
    render() {
        let style = {
            display: "flex",
            msFlexAlign : "row",
            flexWrap : "wrap"
        }
        let pointsStyle=  {
            textAlign: "center",
            // fontSize : "20px"
        }
        return(
<div>
        <div className="controlArea" style={style}>
            <RaisedButton label={"Points : "+this.props.points} disabled={true}/>
            <RaisedButton label="scramble" onClick={this.onScramble.bind(this)} disabled={this.props.solved || this.props.available.length<2 }/>
            <RaisedButton label="submit" onClick={this.onSubmit.bind(this)} disabled={this.props.checking}/>
            <RaisedButton label="clear" onClick={this.onClear.bind(this)} disabled={this.props.checking && this.props.solved}/>
            <RaisedButton label="solve" onClick={this.onSolve.bind(this)} />
        </div>


</div>
    )
    }
}
const mapStateToProps = (state) =>{
    return {
        muiTheme: state.muiTheme,
        word : state.reducerUdd.word,
        available: state.reducerUdd.gameState.available,
        guessed:state.reducerUdd.gameState.guessed,
        points : state.reducerUdd.gameState.points,
        checking : state.reducerUdd.gameState.checking,
        solved : state.reducerUdd.gameState.solved
    }
}
ControlArea.contextTypes = {
    muiTheme : React.PropTypes.object,
    store : React.PropTypes.object,
    word : React.PropTypes.string,
    available : React.PropTypes.array,
    guessed : React.PropTypes.array,
    points : React.PropTypes.number,
    checking : React.PropTypes.bool,
    solved : React.PropTypes.bool,
}
export default connect(mapStateToProps)(ControlArea);