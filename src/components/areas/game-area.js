import React from 'react'
import store from '../../store'
import {connect} from 'react-redux';
import {fetchRandomWord,wordExistRequest} from '../../actions/baseActions'
import LetterComponent from '../letterComponent';

class GuessArea extends React.Component {
    constructor(props){
        super(props)
    }

    componentDidUpdate(){
        if (this.props.guessed.length == this.props.word.length
            && this.props.solved == false ){
             store.dispatch(wordExistRequest({possibleSolution: this.props.guessed.join('')}))

        }
        else if(this.props.solved == true
            && this.props.guessed.length == this.props.word.length )
                setTimeout(()=>store.dispatch(fetchRandomWord()),3000)
    }
    render() {
        let style = {
            display: "flex",
            alignItems : "center"

        }
        return (

            <div className="gameArea" style = {style}>
                {this.props.guessed.map((e,i,a)=> <LetterComponent letter={e} index={i} key={i} type="guess"/>)}

            </div>)
    }
}
const mapStateToProps = (state) =>{
    return {
        muiTheme: state.muiTheme,
        available: state.reducerUdd.gameState.available,
        guessed:state.reducerUdd.gameState.guessed,
        solved : state.reducerUdd.gameState.solved,
        word : state.reducerUdd.gameState.word
    }
}
GuessArea.contextTypes = {
    muiTheme : React.PropTypes.object,
    store : React.PropTypes.object,
    available : React.PropTypes.array,
    guessed : React.PropTypes.array,
    solved : React.PropTypes.bool,
    word: React.PropTypes.string
}
export default connect(mapStateToProps)(GuessArea);