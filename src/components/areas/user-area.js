import React from 'react'
import ReactDOM from 'react-dom'
import dragula from 'react-dragula'
import {connect} from 'react-redux';
import {GET_WORD,fetchData,fetchRandomWordId} from '../../actions/baseActions'
import LetterComponent from '../letterComponent';

class UserArea extends React.Component {
    constructor(props){
        super(props)
    }
    render() {
        let style = {
            display: "flex",
            alignItems : "center",
            flexWrap : "wrap",
            justifyContent: "center"
        }
        return (
            <div className="container userArea" style = {style}>
                {this.props.available.map((e,i,a)=><LetterComponent letter={e} index={i} key={i} type="user"/>)}

            </div>)
    }
    componentDidMount(){
        var container = ReactDOM.findDOMNode(this)
        dragula([container]);
    }
}
const mapStateToProps = (state) =>{
    return {
        muiTheme: state.muiTheme,
        word : state.reducerUdd.word,
        available: state.reducerUdd.gameState.available,
        guessed:state.reducerUdd.gameState.guessed
    }
}
UserArea.contextTypes = {
    muiTheme : React.PropTypes.object,
    store : React.PropTypes.object,
    word : React.PropTypes.string,
    available : React.PropTypes.array,
    guessed : React.PropTypes.array
}
export default connect(mapStateToProps)(UserArea);