/**
 * Created by pengbang on 08.05.2016.
 */
import {createStore, applyMiddleware,combineReducers} from 'redux';
import thunk from 'redux-thunk';
import DevTools from './dev/DevTools';
import baseReducer from './reducers/baseReducer'

let createStoreWithMiddleware = applyMiddleware(thunk)(createStore)

export default createStoreWithMiddleware(baseReducer,DevTools.instrument())