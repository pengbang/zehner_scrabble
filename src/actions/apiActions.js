/**
 * Created by pengbang on 02.05.2016.
 */
import fetch from 'isomorphic-fetch';
import {API_BASE} from '../config';
/**
 * ##########################################
 * #                                        #
 * #    THIS IS THE TEST / BOILERPLATE FILE #
 * #                                        #
 * ##########################################*/
export const API_ALL = 'API_ALL';
export const SELECT_SERVICE = 'SELECT_SERVICE';
export const GET_DATA = 'GET_DATA';
export const TEST = 'TEST';

export function apiAll(state) {
    return Object.assign({}, state, {type: API_ALL})

}


export function selectService(state) {
    return Object.assign({}, state, {type: SELECT_SERVICE})

}
// export function fetchData() {
//     console.log('actionCreator : fetchData')
//     return function(dispatch) {
//         console.log('actionCreator : fetchData : cb')
//         return fetch(API_BASE)
//             .then(response => {
//                 console.log("...in response");
//                 return response.json()})
//             .then(json => {
//                 console.log("fetching/receiving....", json);
//                return dispatch(receiveData(json))
//             })
//     }
// }
// export function receiveData(data) {
//     console.log('actionCreator : receiveData ')
//     return {
//         type: RECEIVE_DATA,
//         payload: {
//             data
//         }
//     }
//
// }
export function getData(data) {
    return {
        type: GET_DATA,
        payload: {
            data
        }
    }

}
export function testData(data) {
    return {
        type: TEST,
        data
    }

}