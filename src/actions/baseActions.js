/**
 * Created by pengbang on 25.05.2016.
 */
import fetch from 'isomorphic-fetch';
import {API_BASE} from '../config'
export const SIDEBAR_OPEN = "SIDEBAR_OPEN";
export const GET_WORD = "GET_WORD";
export const GET_SCRAMBLED = "GET_SCRAMBLED";
export const GET_NEW_ROW = "GET_NEW_ROW";
export const RECEIVE_DATA = "RECEIVE_DATA";
export const GET_WORD_ID = "GET_WORD_ID";
export const RECEIVE_WORD_ID = "RECEIVE_WORD_ID";
export const RECEIVE_WORD = "RECEIVE_WORD";
export const MAKE_GUESS = "MAKE_GUESS";
export const REMOVE_GUESS = "REMOVE_GUESS";
export const SCRAMBLE_AVAILABLE = "SCRAMBLE_AVAILABLE"
export const RECEIVE_SOLVED = "RECEIVE_SOLVED";
export const CLEAR_GAME_AREA = "CLEAR_GAME_AREA";
export const SOLVE_ROUND = "SOLVE_ROUND";
export function fetchData(id) {
    console.log('actionCreator : fetchData')
    return function(dispatch) {
        console.log('actionCreator : fetchData : cb')
        
        return fetch(API_BASE+"word/"+id)
            .then(response => {
                console.log("...in response", response);
                return response.json()})
            .then(json => {
                console.log("fetching/receiving....", json);
               return dispatch(receiveData(json))
            })
    }
}
export function receiveData(data) {
    console.log('actionCreator : receiveData ')
    return {
        type: RECEIVE_WORD,
        payload: {
            data
        }
    }
}
export function fetchWordByWordId() {
    
}
export function fetchRandomWord() {
return dispatch =>
    fetch(API_BASE+'randomId')
    .then(res => res.json())
        .then(json=> dispatch(receiveWordId(json)))
        .then(x =>dispatch(fetchData(x.receivedWordId)))
}
export function receiveWordId(data) {
    console.log("receiveWordId", data)
    return {
        type : RECEIVE_WORD_ID,
        receivedWordId : data
    }

}
export function makeGuess(data) {
    return {
        type : MAKE_GUESS,
        letter : data.letter,
        index : data.index
    }
}
export function removeGuess(data) {
    return {
        type : REMOVE_GUESS,
        letter : data.letter,
        index : data.index
    }
}
export function scrambleAvailable() {
    return {
        type: SCRAMBLE_AVAILABLE
    }
}
export function wordExistRequest(data) {
    console.log("...requsting exist")
    return dispatch =>
    fetch(API_BASE+"exists/"+data.possibleSolution)
        .then(res => res.json())
        .then(json => dispatch(receiveIfSolved(json)))
}
export function receiveIfSolved(data) {
    return {
        type: RECEIVE_SOLVED,
        solved : data.solved
    }
    
}
export function clearGameArea() {
    return {
        type: CLEAR_GAME_AREA
    }    
}
export function solveRound() {
    return {
        type : SOLVE_ROUND
    }
    
}