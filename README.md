# ZEHNER - Scrabble

1. install node.js
2. open terminal in project-folder and run "npm install"
3. run "babel-node server.js"
4. run "npm start"
4. App will be served on "localhost:8000/public" by default.

## DEVELOPMENT NOTES
1. npm install --global webpack-dev-server
2. App requires an instance of the scrabble-server running on localhost:3001 (configurable in ./config.js)

+ Ignore console warning: `Warning: [react-router] You cannot change <Router routes>; it will be ignored` - it is a side effect of webpack-dev-server hot reloading

+ create further Service instance routes like : `/{SERVICE_NAME.toLowerCase()}/{INSTANCE_ID}/{SOME_ATTRIBUTE}`