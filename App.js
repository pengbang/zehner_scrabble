var fs = require('fs');

var data = fs.readFileSync('data/german.dic')

    .toString()
    .split("\r\n");

var words = data
    .filter(function (e, i, a) {
        return e.length == 10
    });
var allWords = data
    .filter(function(e){
        return e.length <= 10
    })

var scrambled = words
    .map(scrambleWord);


//fs.writeFile('data/allUpperCased.dic', allWords.join("\n").toUpperCase());
//fs.writeFile("data/reduced.dic", words.join("\n").toUpperCase());
//fs.writeFile("data/scrambled.dic", scrambled.join("\n").toUpperCase());

function scrambleWord(wordToScramble){
    return wordToScramble.split('').sort().join('')
}
